<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:impl="http://back.bseller.com.br/esigesaasws/services/CPPedido" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ins0="http://wrappedarray/">
  <env:Body>
    <impl:enviaPedido>
      <pedido>
        <cabecalho>
          <clienteent>
            <p_bairro_ent>Brooklin Novo</p_bairro_ent>
            <p_cep_ent>04602000</p_cep_ent>
            <p_cidade_ent>São Paulo</p_cidade_ent>
            <p_compl_ent>Complemento 1</p_compl_ent>
            <p_email_ent>email@isupply.com.br</p_email_ent>
            <p_fax_ent>11 11111111</p_fax_ent>
            <p_id_cliente_ent>31636302866</p_id_cliente_ent>
            <p_id_tipclie_ent>2</p_id_tipclie_ent>
            <p_insest_ent></p_insest_ent>
            <p_logradouro_ent>Rua 1</p_logradouro_ent>
            <p_nome_ent>Wesley Conde</p_nome_ent>
            <p_numero_ent>123</p_numero_ent>
            <p_pais_ent>Brasil</p_pais_ent>
            <p_referencia_ent>Referencia 1</p_referencia_ent>
            <p_tel_celular>11 922222222</p_tel_celular>
            <p_tel_comercial>11 33333333</p_tel_comercial>
            <p_tel_residencial>11 44444444</p_tel_residencial>
            <p_uf_ent>SP</p_uf_ent>
            <p_zip_code_ent></p_zip_code_ent>
          </clienteent>
          <clientefat>
            <p_bairro_fat>Brooklin Novo</p_bairro_fat>
            <p_cep_fat>04602000</p_cep_fat>
            <p_cidade_fat>São Paulo</p_cidade_fat>
            <p_compl_fat>Complemento 1</p_compl_fat>
            <p_email_fat>email@isupply.com.br</p_email_fat>
            <p_fax_fat>11 11111111</p_fax_fat>
            <p_id_clasf_cliente_fat>0</p_id_clasf_cliente_fat>
            <p_id_cliente_fat>31636302866</p_id_cliente_fat>
            <p_id_tipclie_fat>2</p_id_tipclie_fat>
            <p_insest_fat></p_insest_fat>
            <p_logradouro_fat>Rua 1</p_logradouro_fat>
            <p_nome_fat>Wesley Conde</p_nome_fat>
            <p_numero_fat>123</p_numero_fat>
            <p_pais_fat>Brasil</p_pais_fat>
            <p_referencia_fat>Referencia 1</p_referencia_fat>
            <p_tel_celular>11 922222222</p_tel_celular>
            <p_tel_comercial>11 33333333</p_tel_comercial>
            <p_tel_residencial>11 44444444</p_tel_residencial>
            <p_uf_fat>SP</p_uf_fat>
            <p_zip_code_fat></p_zip_code_fat>
          </clientefat>
          <entrega>
            <p_dt_entrega_prom></p_dt_entrega_prom>
            <p_id_periodo_agenda></p_id_periodo_agenda>
            <p_id_tp_entrega>1</p_id_tp_entrega>
          </entrega>
          <p_cif_fob>C</p_cif_fob>
          <p_cod_vendedor></p_cod_vendedor>
          <p_dt_emissao>25/07/2015</p_dt_emissao>
          <p_dt_inclusao>25/07/2015 12:00:00</p_dt_inclusao>
          <p_id_canal>SITE</p_id_canal>
          <p_id_cia>11263</p_id_cia>
          <p_id_lista></p_id_lista>
          <p_in_consumo></p_in_consumo>
          <p_id_unineg>1</p_id_unineg>
          <p_in_pedido>N</p_in_pedido>
          <p_in_quebra></p_in_quebra>
          <p_ip_origem>200.185.249.113</p_ip_origem>
          <p_obs_etiqueta></p_obs_etiqueta>
          <p_origem_interface></p_origem_interface>
          <p_ped_cliente>1509161747</p_ped_cliente>
          <p_ped_externo>B2W-987654321</p_ped_externo>
          <p_ped_loja></p_ped_loja>
          <p_utm_campaign></p_utm_campaign>
          <p_utm_medium></p_utm_medium>
          <p_utm_source></p_utm_source>
          <valores>
            <p_vl_desp_fin>1.00</p_vl_desp_fin>
            <p_vl_despesas>3.00</p_vl_despesas>
            <p_vl_frete>0.00</p_vl_frete>
            <p_vl_merc>100.00</p_vl_merc>
          </valores>
        </cabecalho>
        <item>
          <Item>
            <p_cod_estab_saida>1</p_cod_estab_saida>
            <p_cod_estab_est>3</p_cod_estab_est>
            <p_filial_cnpj>13933305000289</p_filial_cnpj>
            <p_id_contr_transp></p_id_contr_transp>
            <p_id_item>118991</p_id_item>
            <p_id_item_garantido></p_id_item_garantido>
            <p_id_item_kit></p_id_item_kit>
            <p_id_item_pai></p_id_item_pai>
            <p_id_transp></p_id_transp>
            <p_pz_cd>1</p_pz_cd>
            <p_pz_forn>1</p_pz_forn>
            <p_pz_transit>1</p_pz_transit>
            <p_qt>1</p_qt>
            <p_seq>1</p_seq>
            <p_seq_garantido></p_seq_garantido>
            <p_tp_estoque></p_tp_estoque>
            <p_vl_custo></p_vl_custo>
            <p_vl_desc_cond_unit>0</p_vl_desc_cond_unit>
            <p_vl_desc_inc_unit>0</p_vl_desc_inc_unit>
            <p_vl_despesa>2</p_vl_despesa>
            <p_vl_frete>0</p_vl_frete>
            <p_vl_unitario>75.00</p_vl_unitario>
          </Item>
        </item>
        <pagamento>
          <Pagamento>
            <p_bairro>Novo Broklin</p_bairro>
            <p_cartao_6digs>1234****1234</p_cartao_6digs>
            <p_cep>04602000</p_cep>
            <p_cidade>São Paulo</p_cidade>
            <p_cod_seguranca>123</p_cod_seguranca>
            <p_compl>3o andar</p_compl>
            <p_cpf_tit_cartao>31636302866</p_cpf_tit_cartao>
            <p_dt_vencto_bo></p_dt_vencto_bo>
            <p_dt_vencto_cartao>09/2020</p_dt_vencto_cartao>
            <p_id_agencia></p_id_agencia>
            <p_id_banco></p_id_banco>
            <p_id_bandeira_cartao>1</p_id_bandeira_cartao>
            <p_id_conta></p_id_conta>
            <p_id_meio_pagto>1</p_id_meio_pagto>
            <p_id_vale></p_id_vale>
            <p_logradouro>R. Barão do Triunfo</p_logradouro>
            <p_nu_cartao>12345679876543</p_nu_cartao>
            <p_nu_cupom></p_nu_cupom>
            <p_nu_parcelas>1</p_nu_parcelas>
            <p_numero>73</p_numero>
            <p_pais>Brasil</p_pais>
            <p_pe_juros>1</p_pe_juros>
            <p_seq>1</p_seq>
            <p_sit_cod_seguranca>1</p_sit_cod_seguranca>
            <p_titular_cartao>Leonardo D Franco</p_titular_cartao>
            <p_uf>SP</p_uf>
            <p_vl_juros>1</p_vl_juros>
            <p_vl_juros_adm>2</p_vl_juros_adm>
            <p_vl_meio>104.00</p_vl_meio>
          </Pagamento>
        </pagamento>
        <credencial>
          <senha>ZUm50ZWoQjihbmlE0ELxDw==</senha>
          <usuario>RpnAKtPjGWZ0YJ32atSCmg==</usuario>
        </credencial>
      </pedido>
    </impl:enviaPedido>
  </env:Body>
</env:Envelope>

